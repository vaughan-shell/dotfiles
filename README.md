# ~/dotfiles #

All my dotfiles in one place, managed using GNU stow. I have this so that I don't lose all my dotfiles when migrating distributions or computers, and I can easily install them by using stow. If you want to use these dotfiles for yourself, (I don't know why you would, they're not anything special...) install GNU stow and then in the cloned directory run
$ stow (name of program, ie. alacritty)

Thanks to Brandon Invergo for the tutorial on how to use stow to manage dotfiles, here: http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html

